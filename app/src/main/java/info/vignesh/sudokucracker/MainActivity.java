package info.vignesh.sudokucracker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int sCameraId =  Camera.CameraInfo.CAMERA_FACING_BACK;

    private CameraPreview cameraPreview;
    private Camera camera;
    private Context context;
    private OverlayView overlayView;
    private PictureCallback pictureCallback;

    static{
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV load", "couldn't load library");
        }
        System.loadLibrary("jpgt");
        System.loadLibrary("pngt");
        System.loadLibrary("lept");
        System.loadLibrary("tess");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        this.context = getApplicationContext();
        this.overlayView = new OverlayView(context);

        this.cameraPreview = new CameraPreview(this, (SurfaceView) findViewById(R.id.camera_preview));
        this.cameraPreview.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ((RelativeLayout) findViewById(R.id.layout)).addView(cameraPreview);
        this.cameraPreview.setKeepScreenOn(true);

        this.pictureCallback = new PictureCallback(overlayView);

        Button captureButton = (Button) findViewById(R.id.take_picture);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Button Clicked", "button");
                takePicture();
            }
        });

        Toast.makeText(context, getString(R.string.take_photo_help), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        int numCams = Camera.getNumberOfCameras();
        if(numCams > 0){
            try{
                camera = Camera.open(sCameraId);
                camera.startPreview();
                cameraPreview.setCamera(camera);
            } catch (RuntimeException ex){
                Toast.makeText(context, getString(R.string.camera_not_found), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onPause() {
        if(camera != null) {
            camera.stopPreview();
            cameraPreview.setCamera(null);
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    private void takePicture() {
        camera.takePicture(null, null, pictureCallback);
    }
}
