package info.vignesh.sudokucracker;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;

public class OverlayView extends View {
    private final Context mContext;
    private final Paint paint;

    public OverlayView(Context context) {
        super(context);
        mContext = context;

        paint = new Paint();
    }

    // TODO: Create grid lines and preview solution.
}
