package info.vignesh.sudokucracker;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.util.Log;

import info.vignesh.sudokucracker.imageprocessing.SudokuExtractor;
import info.vignesh.sudokucracker.util.FileSaver;
import info.vignesh.sudokucracker.util.SudokuSolver;

public class PictureCallback implements Camera.PictureCallback {

    private final OverlayView overlayView;

    public PictureCallback(OverlayView overlayView) {
        this.overlayView = overlayView;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.d(PictureCallback.class.getName(), "Picture taken.");

        if (data != null) {
            try {
                Log.d(PictureCallback.class.getName(), "decoding to bitmap.");
                Bitmap fullbmp = decodeByteAndScale(data, 5);
                FileSaver.saveImage(fullbmp, "original");

                Log.d(PictureCallback.class.getName(), "decoded data.");
                SudokuExtractor extractor = new SudokuExtractor(fullbmp);
                int[][] unsolved = extractor.extractSudokuNums();

                if (unsolved == null || extractor.isValidGrid()) {
                    Log.d(PictureCallback.class.getName(), "extractSudokuNums Error: returned null");
                } else {
                    int[][] solved = SudokuSolver.solve(unsolved);
                    for (int i = 0; i < 9 ; i++) {
                        for (int j = 0; j < 9; j++) {
                            Log.d("Solution: ", "" + solved[i][j]);
                        }
                    }
                }
            } catch (Exception e) {
                Log.d(PictureCallback.class.getName(), e.getStackTrace().toString());
            }
        }
        camera.startPreview();
    }

    private Bitmap decodeByteAndScale(byte[] data, int scaleBy) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        opts.inSampleSize = scaleBy;
        opts.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(data, 0, data.length, opts);
    }
}
