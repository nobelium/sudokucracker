package info.vignesh.sudokucracker.imageprocessing;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DigitProcessor {

    private int threshholdBtwTileCenters;
    public final static Scalar WHITE = new Scalar(255);
    public final static Scalar BLACK = new Scalar(0);

    public DigitProcessor() {}

    /**
     * Finds the approximate regions of the numbers in the source image
     */
    public List<Rect> getBoundingRects(Mat m) {
        int tileHeight = m.rows() / 9;
        int tileWidth = m.cols() / 9;
        threshholdBtwTileCenters = tileHeight / 2;

        List<Rect> boundRects = new ArrayList<>();

        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(m, contours, new Mat(), Imgproc.RETR_LIST,
                Imgproc.CHAIN_APPROX_SIMPLE);
        for (MatOfPoint mPoint : contours) {
            Rect rect = Imgproc.boundingRect(mPoint);
            if (isNumber(rect.width, rect.height, tileWidth, tileHeight)) {
                rect.width += 2;
                rect.height += 2;
                boundRects.add(rect);
            }
        }
        Log.d("rect count", boundRects.size() + "");
        return boundRects;
    }

    // Finds numbers from Mat image.
    public List<Mat> findCleanNumbers(Mat cleanMat, List<Rect> bounds) {
        List<Mat> numberMats = new ArrayList<>();

        bounds.sort((Rect r1, Rect r2) -> {
            double c1X = r1.x + (r1.width / 2);  // r1 centreX
            double c1Y = r1.y + (r1.height / 2); // r1 centreY
            double c2X = r2.x + (r2.width / 2);  // r2 centreX
            double c2Y = r2.y + (r2.height / 2); // r2 centreY

            // check to see if r1 and r2 are in different rows(y value)
            if (c1Y > c2Y + threshholdBtwTileCenters) {
                return 1;
            } else if (c2Y > c1Y + threshholdBtwTileCenters) {
                return 0;
            }
            // check column (x value) if same row
            else {
                if (c1X > c2X) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        for (Rect rect : bounds) {
            rect = resizeRect(rect, cleanMat, 2);
            Mat numMat = cleanMat.submat(rect);
            removeNoise(numMat);
            numMat = resizeMat(numMat, 1);
            numberMats.add(numMat);
        }
        return numberMats;
    }

    /**
     * Draws the list of filled rects to mat
     */
    public Mat drawRectsToMat(Mat src, List<Rect> rects) {
        Mat result = new Mat(src.size(), src.type());
        for (Rect rect : rects) {
            Imgproc.rectangle(result, new Point(rect.x, rect.y), new Point(rect.x
                    + rect.width, rect.y + rect.height), WHITE, Core.FILLED);
        }
        return result;
    }

    /**
     * Removes small noise from the subMat, keeping only the number.
     */
    private void removeNoise(Mat submat) {
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat tmp = new Mat(submat.size(), submat.type());
        submat.copyTo(tmp);

        Imgproc.findContours(tmp, contours, new Mat(), Imgproc.RETR_LIST,
                Imgproc.CHAIN_APPROX_SIMPLE);
        for (int i = 0; i < contours.size(); i++) {
            Rect r = Imgproc.boundingRect(contours.get(i));
            if (isNoise(r.width, r.height, submat.cols(), submat.rows())) {
                Imgproc.drawContours(submat, contours, i, BLACK, Core.FILLED);
            }
        }
        // FileSaver.storeImage(ImgManipUtil.matToBitmap(result), "d");
        // return result;
    }

    /**
     * Checks whether blob in Mat image is a number.
     */
    private boolean isNumber(int width, int height, int tileWidth,
                             int tileHeight) {
        if (width > 7 * tileWidth / 9 || height > 7 * tileHeight / 9) {
            return false;
        }

        // check this because a number rect should be narrow
        if (width > height) {
            return false;
        }

        // arbitrary parameters to check if rect is too small to be number
        if (height < tileHeight / 3 || width < tileWidth / 6) {
            return false;
        }

        return true;
    }

    /**
     * Checks whether blob in subMat is noise (if small enough)
     */
    private boolean isNoise(int width, int height, int tileWidth, int tileHeight) {
        if (width < tileWidth / 10 || height < tileHeight / 10) {
            return true;
        }
        return false;
    }

    /**
     * Extends and returns Rect by a given constant; checks bounds of Mat first
     */
    private Rect resizeRect(Rect r, Mat mat, int CONST_CROP) {
        int left = r.x;
        int right = r.x + r.width;
        int top = r.y;
        int bot = r.y + r.height;
        if (left - CONST_CROP >= 0) {
            left -= CONST_CROP;
        } else {
            left = 0;
        }

        if (top - CONST_CROP >= 0) {
            top -= CONST_CROP;
        } else {
            top = 0;
        }

        if (right + CONST_CROP < mat.cols()) {
            right += CONST_CROP;
        } else {
            right = mat.cols() - 1;
        }

        if (bot + CONST_CROP < mat.rows()) {
            bot += CONST_CROP;
        } else {
            bot = mat.rows() - 1;
        }

        return new Rect(left, top, right - left, bot - top);
    }

    private Mat resizeMat(Mat m, int resizeBy) {
        if (resizeBy >= m.rows() || resizeBy >= m.cols()) {
            return m;
        }

        int x = resizeBy;
        int y = resizeBy;
        int width = m.cols() - resizeBy;
        int height = m.rows() - resizeBy;

        Rect r = new Rect(x, y, width, height);
        return m.submat(r);
    }
}
