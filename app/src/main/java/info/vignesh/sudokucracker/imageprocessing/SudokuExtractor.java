package info.vignesh.sudokucracker.imageprocessing;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

import info.vignesh.sudokucracker.ocr.TesseractWrapper;
import info.vignesh.sudokucracker.util.FileSaver;
import info.vignesh.sudokucracker.util.ImageManipUtil;

public class SudokuExtractor {

    private final Bitmap imgBitmap;
    private final TesseractWrapper tesseractWrapper;
    private final DigitProcessor digitProcessor;

    private Mat imageMat;
    private Mat cleanImg;
    private boolean isValidGrid;

    public SudokuExtractor(Bitmap imgBitmap) {
        this.imgBitmap = imgBitmap;

        this.digitProcessor = new DigitProcessor();
        this.tesseractWrapper = new TesseractWrapper();
    }

    public boolean isValidGrid() {
        return isValidGrid;
    }

    /**
     * Returns 2d array of numbers from the Sudoku.
     */
    public int[][] extractSudokuNums() {
        imageMat = ImageManipUtil.bitmapToMat(imgBitmap);

        // RGB to Gray scale.
        Imgproc.cvtColor(imageMat, imageMat, Imgproc.COLOR_BGR2GRAY);

        // Imgproc.GaussianBlur(imageMat, imageMat, new Size(11,11), 0);

        Mat grayScaleImg = imageMat;
        Mat extractedMat = extractSudokuGrid(grayScaleImg);
        cleanImg = imageMat.clone();
        FileSaver.saveImage(extractedMat, "extracted");
        if (!isValidGrid) {
            return null;
        }

        // Adaptively assign black and white pixels.
        Imgproc.adaptiveThreshold(
                imageMat, imageMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY_INV, 11, 2);

        FileSaver.saveImage(imageMat, "threshold");

        ImageManipUtil.dilateMat(imageMat, imageMat, 4);
        FileSaver.saveImage(imageMat, "dilated");

        ImageManipUtil.binaryThreshold(imageMat, imageMat);
        FileSaver.saveImage(imageMat, "binaryThreshold");

        List<Rect> boundingRects = digitProcessor.getBoundingRects(extractedMat);

        List<Mat> cleanNumbers = digitProcessor.findCleanNumbers(cleanImg, boundingRects);
        Mat rectMat = digitProcessor.drawRectsToMat(imageMat, boundingRects);
        FileSaver.saveImage(rectMat, "blobext");

        boolean[][] containNums = findNumTiles(rectMat, boundingRects);
        int containCount = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (containNums[i][j]) {
                    containCount++;
                }
            }
        }
        Log.d(SudokuExtractor.class.getName(), "numCount: " + containCount + ", foundCount: " + cleanNumbers.size());
        if (containCount != cleanNumbers.size()) {
            isValidGrid = false;
        }

        int[][] grid = storeNumsToGrid(containNums, cleanNumbers);
        try {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    Log.d(SudokuExtractor.class.getName(), "Grid " + i + ", " + j + ": " + grid[i][j]);
                }
            }
        } finally {
            tesseractWrapper.finalize();
        }
        return grid;
    }

    /**
     * Given an image extracts the un distorted, RGB version of the image.
     */
    private Mat extractSudokuGrid(Mat mat) {
        Mat edges = new Mat(mat.size(), mat.type());
        Imgproc.Canny(mat, edges, 50, 200);

        // Trim external noise to localize the sudoku puzzle and stores in bmp then m2
        int[] bounds = ImageManipUtil.findGridBounds(edges);
        isValidGrid = ImageManipUtil.isSquare(bounds);
        Log.d(SudokuExtractor.class.getName(), "Bounds: " + bounds.length);
        Log.d(SudokuExtractor.class.getName(), "Is Valid grid: " + isValidGrid);

        edges = subMat(edges, bounds);
        imageMat = subMat(imageMat, bounds);

        Log.d(SudokuExtractor.class.getName(), "Finding Corners: " + edges.rows() + " " + edges.cols());
        List<Point> corners = findCorners(edges);
        Point topLeft = corners.get(0);
        Point topRight = corners.get(1);
        Point bottomLeft = corners.get(2);
        Point bottomRight = corners.get(3);

        Log.d(SudokuExtractor.class.getName(), "Top left: " + topLeft.toString());
        Log.d(SudokuExtractor.class.getName(), "Top Right: " + topRight.toString());
        Log.d(SudokuExtractor.class.getName(), "Bottom left: " + bottomLeft.toString());
        Log.d(SudokuExtractor.class.getName(), "Bottom Right: " + bottomRight.toString());

        edges = ImageManipUtil.fixPerspective(topLeft, topRight, bottomLeft,
                bottomRight, edges);
        imageMat = ImageManipUtil.fixPerspective(topLeft, topRight, bottomLeft,
                bottomRight, imageMat);
        FileSaver.saveImage(edges, "edges");
        FileSaver.saveImage(imageMat, "fixedPerspective");

        return edges;
    }

    /**
     * Closer crop of the grid.
     */
    private Mat subMat(Mat mat, int[] bounds) {
        int left = bounds[0];
        int right = bounds[1];
        int top = bounds[2];
        int bot = bounds[3];

        return mat.submat(top, bot, left, right);
    }

    /**
     * Finds corners of the sudoku grid in the Mat image using openCV HoughLines
     * points of intersection.
     */
    private List<Point> findCorners(Mat mat) {
        Mat lines = new Mat();
        List<double[]> horizontalLines = new ArrayList<>();
        List<double[]> verticalLines = new ArrayList<>();

        Imgproc.HoughLinesP(mat, lines, 1, Math.PI / 180, 200);

        Log.d("Found line: ", lines.rows() + " " + lines.cols());

        for (int i = 0; i < lines.rows(); i++) {
            double[] line = lines.get(i, 0);
            double x1 = line[0];
            double y1 = line[1];
            double x2 = line[2];
            double y2 = line[3];
            if (Math.abs(y2 - y1) < Math.abs(x2 - x1)) {
                horizontalLines.add(line);
            } else if (Math.abs(x2 - x1) < Math.abs(y2 - y1)) {
                verticalLines.add(line);
            }
        }
        Log.d(SudokuExtractor.class.getName(), String.format(
                "Lines total: %d, horizontal: %d, vertical: %d",
                horizontalLines.size(), verticalLines.size(), lines.rows()));

        // FindParentFor the lines furthest from centre which will be the bounds for the grid
        double[] topLine = horizontalLines.get(0);
        double[] bottomLine = horizontalLines.get(0);
        double[] leftLine = verticalLines.get(0);
        double[] rightLine = verticalLines.get(0);

        double xMin = 1000;
        double xMax = 0;
        double yMin = 1000;
        double yMax = 0;

        for (int i = 0; i < horizontalLines.size(); i++) {
            if (horizontalLines.get(i)[1] < yMin
                    || horizontalLines.get(i)[3] < yMin) {
                topLine = horizontalLines.get(i);
                yMin = horizontalLines.get(i)[1];
            } else if (horizontalLines.get(i)[1] > yMax
                    || horizontalLines.get(i)[3] > yMax) {
                bottomLine = horizontalLines.get(i);
                yMax = horizontalLines.get(i)[1];
            }
        }

        for (int i = 0; i < verticalLines.size(); i++) {
            if (verticalLines.get(i)[0] < xMin
                    || verticalLines.get(i)[2] < xMin) {
                leftLine = verticalLines.get(i);
                xMin = verticalLines.get(i)[0];
            } else if (verticalLines.get(i)[0] > xMax
                    || verticalLines.get(i)[2] > xMax) {
                rightLine = verticalLines.get(i);
                xMax = verticalLines.get(i)[0];
            }
        }

        // obtain four corners of sudoku grid
        Point topLeft = ImageManipUtil.findCorner(topLine, leftLine);
        Point topRight = ImageManipUtil.findCorner(topLine, rightLine);
        Point bottomLeft = ImageManipUtil.findCorner(bottomLine, leftLine);
        Point bottomRight = ImageManipUtil.findCorner(bottomLine, rightLine);

        return Arrays.asList(topLeft, topRight, bottomLeft, bottomRight);
    }

    /**
     * Finds which tile contains a number and which doesn't.
     */
    private boolean[][] findNumTiles(Mat m, List<Rect> rects) {
        byte[][] arrayMat = addRectsToMat(m, rects);
        boolean[][] numTileArray = new boolean[9][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                numTileArray[i][j] = containsNumberTile(arrayMat, j, i);
            }
        }
        return numTileArray;
    }

    private byte[][] addRectsToMat(Mat m, List<Rect> nums) {
        byte[][] matArray = new byte[m.rows()][m.cols()];

        for (Rect r : nums) {
            for (int y = r.y; y < r.y + r.height - 1; y++) {
                for (int x = r.x; x < r.x + r.width - 1; x++) {
                    // set to 1 (white)
                    matArray[y][x] = 1;
                }
            }
        }
        return matArray;
    }

    /**
     * Determines if array holding mat contains a number,
     */
    private boolean containsNumberTile(byte[][] matArray, int xBound, int yBound) {
        int totalWhite = 0;
        int xStart = xBound * matArray[0].length / 9;
        int xEnd = xStart + matArray[0].length / 9 - 5;
        int yStart = yBound * matArray.length / 9;
        int yEnd = yStart + matArray.length / 9 - 5;

        for (int y = yStart; y < yEnd; y++) {
            for (int x = xStart; x < xEnd; x++) {
                if (matArray[y][x] == 1) {
                    totalWhite++;
                }
            }
        }
        int area = (xEnd - xStart) * (yEnd - yStart);
        return totalWhite > (area / 10);
    }

    /**
     * Uses OCR to find the number in tile and stores results in 2D array.
     */
    public int[][] storeNumsToGrid(boolean[][] isNumPresent, List<Mat> nums) {
        int[][] grid = new int[9][9];
        int numIndex = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (isNumPresent[i][j] == true) {
                    Log.d(SudokuExtractor.class.getName(), "Detected num at: " + i + "_" + j);
                    Mat num = nums.get(numIndex);
                    FileSaver.saveImage(num, i + "_" + j);
                    grid[i][j] = getOCRNum(num);
                    Log.d(SudokuExtractor.class.getName(), "OCR num at: " + i + "_" + j + " as " + grid[i][j]);
                    numIndex++;
                } else {
                    Log.d(SudokuExtractor.class.getName(), "Did not detect num at: " + i + "_" + j);
                }
            }
        }
        return grid;
    }

    /**
     * Uses tessOCR to recognize the digit in the Mat.
     */
    private int getOCRNum(Mat num) {
        Bitmap b = ImageManipUtil.matToBitmap(num);
        int ans = Integer.parseInt(tesseractWrapper.doOCR(b)) % 10;
        return ans;
    }
}
