package info.vignesh.sudokucracker.ocr;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

public class TesseractWrapper {

    private TessBaseAPI tessBaseAPI;
    private final String dataPath;

    public TesseractWrapper() {
        // Manually copy eng.traineddata to /sdcard/tessdata/
        dataPath = "/sdcard/";
        initOCR();
    }

    private void initOCR() {
        tessBaseAPI = new TessBaseAPI();

        // datapath is in parent directory of tessdata
        tessBaseAPI.init(dataPath, "eng");
        tessBaseAPI.setVariable("tessedit_char_whitelist", "0123456789");
    }

    public String doOCR(Bitmap bmp) {
        tessBaseAPI.setImage(bmp);
        String result = tessBaseAPI.getUTF8Text();
        return result;
    }

    public void finalize() {
        tessBaseAPI.end();
    }
}
