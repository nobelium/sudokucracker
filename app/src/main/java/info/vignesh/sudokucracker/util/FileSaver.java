package info.vignesh.sudokucracker.util;

import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;

import org.opencv.core.Mat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileSaver {

    public static void saveImage(byte[] data, String postfixName) {
        File pictureFile = getOutputMediaFile(postfixName);
        if (pictureFile == null) {
            return;
        }
        FileOutputStream fos = null;

        Log.d("Saving to file", "d");
        try {
            fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
            Log.d("Saved to file", "d");

            ExifInterface exif = new ExifInterface(pictureFile.getAbsolutePath());
            String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);

            Log.d("File exif orientation: ", exifOrientation);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveImage(Mat mat, String postfixName) {
        saveImage(ImageManipUtil.matToBitmap(mat), postfixName);
    }

    public static void saveImage(Bitmap image, String postfixName) {
        if (image == null) {
            Log.d("null bitmap", "storeImage");
        }

        Log.d("Saving ", postfixName);
        File pictureFile = getOutputMediaFile(postfixName);
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File getOutputMediaFile(String postfix) {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + "_" + postfix + ".jpg");

        return mediaFile;
    }
}
