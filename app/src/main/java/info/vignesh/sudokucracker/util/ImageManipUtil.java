package info.vignesh.sudokucracker.util;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.List;

public class ImageManipUtil {

    public static final int IS_SQUARE_THRESHOLD = 25;

    public static final String TAG_ERROR_FIND_GRID = "findGridArea error";

    /**
     * Converts Bitmap image to OpenCV Mat
     */
    public static Mat bitmapToMat(Bitmap bmp) {
        Mat mat = new Mat(bmp.getHeight(), bmp.getWidth(), CvType.CV_8UC1);
        Utils.bitmapToMat(bmp, mat);

        String matInfo = String.format("cols: %d, rows: %d", mat.cols(),
                mat.rows(), mat.channels());
        Log.d("Mat dimensions", matInfo);

        return mat;
    }

    /**
     * Trims the bitmap to contain only the sudoku grid, input is edges of the main image.
     *
     * if sides differ by more than threshold amount of pixels, then
     * throw error since area is not square if(Math.abs(right - left -
     * (bot - top)) > THRESHOLD){ Log.d(TAG_ERROR_FIND_GRID, "not square");
     * }
     *
     * Bitmap subBmp = Bitmap.createBitmap(bmp, left, top, right-left,
     * bot-top); Mat subMat = mat.submat(top, bot, left, right); String
     * subMatInfo = String.format("left: %d, right: %d, top: %d, bot: %d",
     * left, right, top, bot); Log.d(TAG_SUBMAT_DIMENS, subMatInfo);
     *
     * return subMat; //return subBmp; *
     */
    public static int[] findGridBounds(Mat mat) {
        int[] bounds = new int[4];
        // 5px buffer for grid lines.
        int left = findBorders(1, mat) - 5;
        int right = findBorders(2, mat) + 5;
        int top = findBorders(3, mat) - 5;
        int bot = findBorders(4, mat) + 5;

        bounds[0] = left;
        bounds[1] = right;
        bounds[2] = top;
        bounds[3] = bot;

        return bounds;

    }

    /**
     * Find border of the sudoku grid.
     *
     * The edges are in white and the rest is black.
     * The search for white line begins from 1/3 length away from the center of the grid.
     *
     * side: 1=left, 2=right, 3=top, 4=bottom
     * Returns negative if side is not one of the above.
     */
    private static int findBorders(int side, Mat mat) {
        switch (side) {
            case 1:  // left
                for (int i = mat.cols() / 3; i > 0; i--) {
                    if (isBorderHeight(i, mat))
                        return i;
                }
                break;
            case 2:  // right
                for (int i = 2 * mat.cols() / 3; i < mat.cols(); i++) {
                    if (isBorderHeight(i, mat))
                        return i;
                }
                break;
            case 3:  // top
                for (int i = mat.rows() / 3; i > 0; i--) {
                    if (isBorderWidth(i, mat))
                        return i;
                }
                break;
            case 4:  // bottom
                for (int i = 2 * mat.rows() / 3; i < mat.rows(); i++) {
                    if (isBorderWidth(i, mat))
                        return i;
                }
                break;
            default:
                Log.d(TAG_ERROR_FIND_GRID, "boundary not found: side " + side);
        }
        return -6;
    }

    /**
     * Checks if the white horizontal line is outside the sudoku grid.
     */
    private static boolean isBorderWidth(int height, Mat mat) {
        for (int i = 2 * mat.cols() / 5; i < 3 * mat.cols() / 5; i++) {
            // if pixel is black
            if ((int) mat.get(height, i)[0] == 255) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the white vertical line is outside the sudoku grid.
     */
    private static boolean isBorderHeight(int width, Mat mat) {
        for (int i = 2 * mat.rows() / 5; i < 3 * mat.rows() / 5; i++) {
            // if pixel is black
            if ((int) mat.get(i, width)[0] == 255) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the bound for a grid form a square.
     */
    public static boolean isSquare(int[] bounds) {
        int left = bounds[0];
        int right = bounds[1];
        int top = bounds[2];
        int bot = bounds[3];

        // Consider the grid to be a square even if it's a rectangle with threshold checks.
        if (Math.abs(right - left - (bot - top)) > IS_SQUARE_THRESHOLD) {
            Log.d(TAG_ERROR_FIND_GRID, "not square");
            return false;
        }
        return true;
    }

    /**
     * Returns point of intersection between two lines.
     *
     * Note: will result in Runtime exception if parallel lines are passed.
     */
    public static Point findCorner(double[] l1, double[] l2) {
        double x1 = l1[0];
        double y1 = l1[1];
        double x2 = l1[2];
        double y2 = l1[3];
        double x3 = l2[0];
        double y3 = l2[1];
        double x4 = l2[2];
        double y4 = l2[3];

        double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        double x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2)
                * (x3 * y4 - y3 * x4))
                / d;
        double y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2)
                * (x3 * y4 - y3 * x4))
                / d;

        Point p = new Point(x, y);
        return p;
    }

    /**
     * Removes distortion from the Sudoku Grid.
     *
     * Four corners from the image will be mapped to the desired four corners in the output. Open
     * CV finds the necessary transformation and applies the transformation on the input image.
     */
    public static Mat fixPerspective(
            Point upLeft, Point upRight, Point downLeft, Point downRight, Mat source) {
        List<Point> src = new ArrayList<>();
        List<Point> dest = new ArrayList<>();
        Mat result = new Mat(source.size(), source.type());

        // add the four corners to List
        src.add(upLeft);
        src.add(upRight);
        src.add(downLeft);
        src.add(downRight);

        Point topLeft = new Point(0, 0);
        Point topRight = new Point(source.cols(), 0);
        Point bottomLeft = new Point(0, source.rows());
        Point bottomRight = new Point(source.cols(), source.rows());

        // add destination corners to List (adjusted for rotation)
        dest.add(topRight);
        dest.add(bottomRight);
        dest.add(topLeft);
        dest.add(bottomLeft);

        // convert List to Mat
        Mat srcM = Converters.vector_Point2f_to_Mat(src);
        Mat destM = Converters.vector_Point2f_to_Mat(dest);

        // apply perspective transform using 3x3 matrix
        Mat perspectiveTrans = new Mat(3, 3, CvType.CV_32FC1);
        perspectiveTrans = Imgproc.getPerspectiveTransform(srcM, destM);
        Imgproc.warpPerspective(source, result, perspectiveTrans, result.size());

        return result;
    }

    /**
     * Does OpenCV dilation on the Mat.
     */
    public static void dilateMat(Mat src, Mat dst, int factor) {
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS,
                new Size(factor, factor));
        Imgproc.dilate(src, dst, kernel);
    }

    /**
     * Does OpenCV thresholding.
     */
    public static void binaryThreshold(Mat src, Mat dst) {
        Imgproc.threshold(src, dst, 128, 255, Imgproc.THRESH_BINARY);
    }

    /**
     * Converts mat to RGB bitmap.
     */
    public static Bitmap matToBitmap(Mat mat) {
        Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(),
                Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bmp);

        String bmpInfo = String.format("width: %d, height %d", bmp.getWidth(),
                bmp.getHeight());
        Log.d("Bitmap dimensions", bmpInfo);

        return bmp;
    }
}
