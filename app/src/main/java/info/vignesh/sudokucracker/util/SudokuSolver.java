package info.vignesh.sudokucracker.util;

public class SudokuSolver {
    public static int[][] solve(int[][] a) {
        int[][] b = a.clone();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (a[i][j] != 0 && !checkValid(a, i, j)) {
                    return b;
                }
            }
        }
        solve(b, 0, 0);
        return b;
    }

    private static boolean solve(int[][] a, int i, int j) {
        if (i==3) return true;

        int nextI = getNextX(i, j);
        int nextJ = getNextY(i, j);
        if(a[i][j] == 0) {
            for (int k = 1; k <= 9; k++) {
                a[i][j] = k;
                if (checkValid(a, i, j)) {
                    if (solve(a, nextI, nextJ)) {
                        return true;
                    }
                }
            }
            a[i][j] = 0;
            return false;
        }

        return solve(a, nextI, nextJ);
    }

    private static int getNextX(int x, int y) {
        if (y==2) return x+1;
        return x;
    }

    private static int getNextY(int x, int y) {
        if (y==2) return 0;
        return y+1;
    }

    private static boolean checkValid(int[][] a, int i, int j) {
        for (int x = 0; x < 9; x++) {
            if ((a[i][x] == a[i][j] && x != j) || (a[x][j] == a[i][j] && x != i)) {
                return  false;
            }
        }
        int qx = (i/3)*3;
        int qy = (j/3)*3;
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                if (a[i][j] == a[qx + x][qy + y] && (i != qx + x || j != qy + y)) {
                    return false;
                }
            }
        }
        return true;
    }
}
